---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Exchange Interactions in materials: Can we calculate/estimate them?"
event:
event_url: https://quantumsummer.epfl.ch/epfl-ethz/
location: EPFL
address:
  street:
  city: Lausanne
  region:
  postcode:
  country: Switzerland
summary:
abstract: In this lecture an overview of reliable methods to compute exchange interactions in quantum magnets is presented.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-08-21T09:00:00+02:00
date_end: 2019-08-21T10:30:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2019-09-01T16:46:11+02:00

authors: [Vamshi M. Katukuri]
tags: []

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: "SummerSchool_QM_2019_lecture.pdf"
url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
