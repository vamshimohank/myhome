---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Understanding electronic correlations in oxides: Ab initio wave function calculations"
event: "MPI-FKF institute scientific meeting"
event_url: 
location: MPI-FKF
address:
  street:
  city: Stuttgart
  region:
  postcode:
  country: Germany
summary:
abstract: How wave function based methods enable to visualize electronic correlations in oxides is explained with nickelate and cuprate superconductors as examples.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2021-12-10T09:30:00+00:00
date_end: 2021-12-10T10:00:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2021-12-11T16:46:11+02:00

authors: [Vamshi M. Katukuri]
tags: []

# Is this a featured talk? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: "FKF_directors_meeting_Dec10_2021_last.pdf"
url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
