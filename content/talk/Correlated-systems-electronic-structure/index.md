---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Electronic structure of strongly correlated materials"
event: Solid state physics - 4, Guest lecture
event_url:
location: EPFL
address:
  street:
  city: Lausanne
  region:
  postcode:
  country: Switzerland
summary:
abstract: It is a guest lecture covering the {\it ab initio} methods for studying the electronic structure of strongly correlated  materials.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2018-05-30T15:30:00+02:00
date_end: 2018-05-30T17:00:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-04-30T17:45:23+02:00

authors: [Vamshi M. Katukuri]
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: CorrelatedMaterialsLecture.pdf

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
