+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Scientist"
  company = "Fraunhofer IAO"
  company_url = ""
  location = "Stuttgart, Germany"
  date_start = "2023-04-01"
  date_end = ""
  description = """
  * Investigation of quantum algorithms for price prediction of financial derivatives
  * Execution of quantum algorithms on quantum Hardware 
  * Develop quantum algorithms for solving molecular Hamiltonians
  * Design and execute training program in quantum computing technology
  * Integration of Quantum computing technologies in to existing high-performance computing infrastructure
  * """

[[experience]]
  title = "Scientist"
  company = "MPI-FKF"
  company_url = ""
  location = "Stuttgart, Germany"
  date_start = "2019-04-01"
  date_end = "2023-03-21"
  description = """ 
  * Designing scientific projects in the field of strongly correlated Fermionic systems
  * Understanding the mechanism of magnetic exchange in nickelate superconductors 
  * Full Configuration Interaction Quantum Monte Carlo investigation of frustrated Heisenberg model
    """

[[experience]]
  title = "Postdoc Scientist"
  company = "EPFL"
  company_url = ""
  location = "Lausanne, Switzerland"
  date_start = "2015-02-15"
  date_end = "2019-03-31"
  description = """ 
  * Spin-liquid like states in double perovskite d10-d9 cuprate systems
  * Design of high-throughput protocal for design and discovery of materials with Kitaev exchange
  * Investigation of light induced electron-phonon interactions in magnetite with Density Fuctional Perturbation theory
  """

[[experience]]
  title = "Doctoral research student"
  company = "IFW-Dresden"
  company_url = ""
  location = "Dresden"
  date_start = "2011-04-01"
  date_end = "2014-12-31"
  description = """ 
  * Quantum chemical methods for solid state materials
  * Electronic structure of iridates
  * Material realization of Kitaev-Heisenberg model 
  """

[[experience]]
  title = "Research and teaching assistant"
  company = "University of Minnesota"
  company_url = ""
  location = "Minneapolis, USA"
  date_start = "2009-08-01"
  date_end = "2011-03-31"
  description = """ """
+++
