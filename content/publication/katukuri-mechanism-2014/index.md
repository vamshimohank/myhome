---
title: "Mechanism of Basal-Plane Antiferromagnetism in the Spin-Orbit Driven Iridate Ba 2 IrO 4"
date: 2014-06-01
publishDate: 2020-04-29T14:06:00.736684Z
authors: ["Vamshi M. Katukuri", "Viktor Yushankhai", "Liudmila Siurakshina", "Jeroen van den Brink", "Liviu Hozoi", "Ioannis Rousochatzakis"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. X*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevX.4.021051"
categories: [Iridates]
tags: [Anisotropic Exchange]
doi: "10.1103/PhysRevX.4.021051"
---

