---
title: "Possibility of an unconventional spin state of Ir 4 + in Ba 21 Ir 9 O 43 single crystal"
date: 2016-09-01
publishDate: 2020-04-29T14:06:00.741266Z
authors: ["L. Yang", "M. Jeong", "A. Arakcheeva", "I. Živković", "B. Náfrádi", "A. Magrez", "A. Pisoni", "J. Jacimovic", "V. M. Katukuri", "S. Katrych", "N. E. Shaik", "O. V. Yazyev", "L. Forró", "H. M. Rønnow"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. B*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevB.94.104403"
doi: "10.1103/PhysRevB.94.104403"
---

