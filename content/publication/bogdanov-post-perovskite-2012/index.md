---
title: "Post-perovskite CaIrO$_3$ : A $j=1/2$ quasi-one-dimensional antiferromagnet"
date: 2012-06-01
publishDate: 2020-04-29T14:06:00.733322Z
authors: ["Nikolay A. Bogdanov", "Vamshi M. Katukuri", "Hermann Stoll", "Jeroen van den Brink", "Liviu Hozoi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. B*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevB.85.235147"
doi: "10.1103/PhysRevB.85.235147"
---

