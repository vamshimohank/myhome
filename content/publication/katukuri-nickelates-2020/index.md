---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Electronic correlations and magnetic interactions in infinite-layer NdNiO$_2$"
authors: [Vamshi M. Katukuri, Nikolay A. Bogdanov, Oskar Weser, Jeroen van den Brink, and Ali Alavi]
date: 2020-12-17
doi: "10.1103/PhysRevB.102.241112"

# Schedule page publish date (NOT publication's date).
publishDate: 2021-01-28T21:48:31+01:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Phys. Rev. B(R)*"
publication_short: ""

abstract: ""

# Summary. An optional shortened abstract.
summary: ""

tags: [Exchange Interactions, Electron correlations]
categories: [Nickelates]
featured: true

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "https://journals.aps.org/prb/pdf/10.1103/PhysRevB.102.241112"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
