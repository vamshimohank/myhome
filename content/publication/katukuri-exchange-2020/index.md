---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Exchange Interactions Mediated by Nonmagnetic Cations in Double Perovskites"
authors: ["Vamshi M. Katukuri", "P. Babkevich", "O. Mustonen", "H. C. Walker", "B. Fåk", "S. Vasala", "M. Karppinen", "H. M. Rønnow", "O. V. Yazyev"]
date: 2020-02-01
doi: "10.1103/PhysRevLett.124.077202"

# Schedule page publish date (NOT publication's date).
publishDate: 2020-04-29T14:06:00.744811Z

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: "*Phys. Rev. Lett.*"
publication_short: ""

abstract: ""

# Summary. An optional shortened abstract.
summary: ""

featured: true
categories: [Double perovskites, QSL]
tags: [Spin Waves, Quantum Spin Liquids, Exchange interactions, Double perovskites]

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf: "https://link.aps.org/doi/10.1103/PhysRevLett.124.077202"
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
