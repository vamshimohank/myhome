---
title: "Magnetic Excitations and Electronic Interactions in Sr 2 CuTeO 6 : A Spin- 1 / 2 Square Lattice Heisenberg Antiferromagnet"
date: 2016-12-01
publishDate: 2020-04-29T14:06:00.740566Z
authors: ["P. Babkevich", "Vamshi M. Katukuri", "B. Fåk", "S. Rols", "T. Fennell", "D. Pajić", "H. Tanaka", "T. Pardini", "R. R. P. Singh", "A. Mitrushchenkov", "O. V. Yazyev", "H. M. Rønnow"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. Lett.*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevLett.117.237203"
categories: [Double perovskites]
tags: [Spin Waves, Exchange interactions, Double perovskites, MRCISD]
doi: "10.1103/PhysRevLett.117.237203"
---

