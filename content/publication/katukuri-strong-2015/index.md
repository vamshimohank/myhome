---
title: "Strong magnetic frustration and anti-site disorder causing spin-glass behavior in honeycomb Li2RhO3"
date: 2015-12-01
publishDate: 2020-04-29T14:06:00.737991Z
authors: ["Vamshi M. Katukuri", "Satoshi Nishimoto", "Ioannis Rousochatzakis", "Hermann Stoll", "Jeroen van den Brink", "Liviu Hozoi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Sci Rep*"
url_pdf: "http://www.nature.com/articles/srep14718"
doi: "10.1038/srep14718"
---

