---
title: "Electronic Structure of Low-Dimensional 4$d$ and 5$d$ Oxides: Interplay of Ligand Distortions, Overall Lattice Anisotropy, and Spin–Orbit Interactions"
date: 2014-05-01
publishDate: 2020-04-29T14:06:00.743976Z
authors: ["Vamshi M. Katukuri", "Karla Roszeitis", "Viktor Yushankhai", "Alexander Mitrushchenkov", "Hermann Stoll", "Michel van Veenendaal", "Peter Fulde", "Jeroen van den Brink", and "Liviu Hozoi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Inorganic Chemistry*"
url_pdf: "https://pubs.acs.org/doi/10.1021/ic402653f"
categories: [Iridates]
tags: [4d and 5d oxides, d-d excitations, crystal fields]
doi: "10.1021/ic402653f"
---

