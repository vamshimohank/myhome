---
title: "Testing the Validity of the Strong Spin-Orbit-Coupling Limit for Octahedrally Coordinated Iridate Compounds in a Model System Sr 3 CuIrO 6"
date: 2012-10-01
publishDate: 2020-04-29T14:06:00.733957Z
authors: ["X. Liu", "Vamshi M. Katukuri", "L. Hozoi", "Wei-Guo Yin", "M. P. M. Dean", "M. H. Upton", "Jungho Kim", "D. Casa", "A. Said", "T. Gog", "T. F. Qi", "G. Cao", "A. M. Tsvelik", "Jeroen van den Brink", "J. P. Hill"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. Lett.*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevLett.109.157401"
doi: "10.1103/PhysRevLett.109.157401"
---

