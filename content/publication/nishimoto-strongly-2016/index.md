---
title: "Strongly frustrated triangular spin lattice emerging from triplet dimer formation in honeycomb Li2IrO3"
date: 2016-04-01
publishDate: 2020-04-29T14:06:00.737368Z
authors: ["Satoshi Nishimoto", "Vamshi M. Katukuri", "Viktor Yushankhai", "Hermann Stoll", "Ulrich K. Rößler", "Liviu Hozoi", "Ioannis Rousochatzakis", "Jeroen van den Brink"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Nat Commun*"
url_pdf: "http://www.nature.com/articles/ncomms10273"
categories: [Iridates]
tags: [Kitaev Model, Kitaev interactions, Exact Diagonalization]
doi: "10.1038/ncomms10273"
---

