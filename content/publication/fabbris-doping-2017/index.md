---
title: "Doping Dependence of Collective Spin and Orbital Excitations in the Spin-1 Quantum Antiferromagnet La 2 − x Sr x NiO 4 Observed by X Rays"
date: 2017-04-01
publishDate: 2020-04-29T14:06:00.741898Z
authors: ["G. Fabbris", "D. Meyers", "L. Xu", "V. M. Katukuri", "L. Hozoi", "X. Liu", "Z.-Y. Chen", "J. Okamoto", "T. Schmitt", "A. Uldry", "B. Delley", "G. D. Gu", "D. Prabhakaran", "A. T. Boothroyd", "J. van den Brink", "D. J. Huang", "M. P. M. Dean"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. Lett.*"
url_pdf: "http://link.aps.org/doi/10.1103/PhysRevLett.118.156402"
doi: "10.1103/PhysRevLett.118.156402"
---

