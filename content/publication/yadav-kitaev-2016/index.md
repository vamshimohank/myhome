---
title: "Kitaev exchange and field-induced quantum spin-liquid states in honeycomb α-RuCl3"
date: 2016-12-01
publishDate: 2020-04-29T14:06:00.739210Z
authors: ["Ravi Yadav", "Nikolay A. Bogdanov", "Vamshi M. Katukuri", "Satoshi Nishimoto", "Jeroen van den Brink", "Liviu Hozoi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Sci Rep*"
url_pdf: "http://www.nature.com/articles/srep37925"
categories: [Kitaev]
tags: [RuCl3, Kitaev interactions]
doi: "10.1038/srep37925"
---

