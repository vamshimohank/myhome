---
title: "Ab initio determination of excitation energies and magnetic couplings in correlated quasi-two-dimensional iridates"
date: 2012-06-01
publishDate: 2020-04-29T14:06:00.729292Z
authors: ["Vamshi M. Katukuri", "Hermann Stoll", "Jeroen van den Brink", and "Liviu Hozoi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. B*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevB.85.220402"
categories: [Iridates]
tags: [Iridates, d-d excitations, Sr2IrO4, Quantum Chemistry]
doi: "10.1103/PhysRevB.85.220402"
---

