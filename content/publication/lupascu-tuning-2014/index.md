---
title: "Tuning Magnetic Coupling in Sr$_2$IrO$_4$ Thin Films with Epitaxial Strain"
date: 2014-04-01
publishDate: 2020-04-29T14:06:00.735398Z
authors: ["A. Lupascu", "J. P. Clancy", "H. Gretarsson", "Zixin Nie", "J. Nichols", "J. Terzic", "G. Cao", "S. S. A. Seo", "Z. Islam", "M. H. Upton", "Jungho Kim", "D. Casa", "T. Gog", "A. H. Said", "Vamshi M. Katukuri", "H. Stoll", "L. Hozoi", "J. van den Brink", and "Young-June Kim"]
publication_types: ["2"]
abstract: ""
featured: false
publication: " Phys. Rev. Lett."
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevLett.112.147201"
doi: "10.1103/PhysRevLett.112.147201"
---

