---
title: "Orbital reconstruction in nonpolar tetravalent transition-metal oxide layers"
date: 2015-11-01
publishDate: 2020-04-29T14:06:00.739932Z
authors: ["Nikolay A. Bogdanov", "Vamshi M. Katukuri", "Judit Romhányi", "Viktor Yushankhai", "Vladislav Kataev", "Bernd Büchner", "Jeroen van den Brink", "Liviu Hozoi"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Nat Commun*"
url_pdf: "http://www.nature.com/articles/ncomms8306"
categories: [Iridates]
tags: [DM interaction, g-tensors,Anisotropic Exchange]
doi: "10.1038/ncomms8306"
---

