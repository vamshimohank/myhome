---
title: "Crystal-Field Splitting and Correlation Effect on the Electronic Structure of A 2 IrO 3"
date: 2013-02-01
publishDate: 2020-04-29T14:06:00.734767Z
authors: ["H. Gretarsson", "J. P. Clancy", "X. Liu", "J. P. Hill", "Emil Bozin", "Yogesh Singh", "S. Manni", "P. Gegenwart", "Jungho Kim", "A. H. Said", "D. Casa", "T. Gog", "M. H. Upton", "Heung-Sik Kim", "J. Yu", "Vamshi M. Katukuri", "L. Hozoi", "Jeroen van den Brink", "Young-June Kim"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. Lett.*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevLett.110.076402"
doi: "10.1103/PhysRevLett.110.076402"
---

