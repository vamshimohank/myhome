---
title: "Observation of heavy spin-orbit excitons propagating in a nonmagnetic background: The case of ( Ba , Sr ) 2 YIrO 6"
date: 2018-02-01
publishDate: 2020-04-29T14:06:00.743208Z
authors: ["M. Kusch", "V. M. Katukuri", "N. A. Bogdanov", "B. Büchner", "T. Dey", "D. V. Efremov", "J. E. Hamann-Borrero", "B. H. Kim", "M. Krisch", "A. Maljuk", "M. Moretti Sala", "S. Wurmehl", "G. Aslan-Cansever", "M. Sturza", "L. Hozoi", "J. van den Brink", "J. Geck"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Physical Review B*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevB.97.064421"
doi: "10.1103/PhysRevB.97.064421"
---

