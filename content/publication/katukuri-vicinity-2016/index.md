---
title: "The vicinity of hyper-honeycomb β-Li2IrO3 to a three-dimensional Kitaev spin liquid state"
date: 2016-09-01
publishDate: 2020-04-29T14:06:00.738589Z
authors: ["Vamshi M. Katukuri", "Ravi Yadav", "Liviu Hozoi", "Satoshi Nishimoto", "Jeroen van den Brink"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Sci Rep*"
url_pdf: "http://www.nature.com/articles/srep29585"
doi: "10.1038/srep29585"
---

