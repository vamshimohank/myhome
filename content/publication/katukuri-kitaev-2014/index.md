---
title: "Kitaev interactions between j = 1/2 moments in honeycomb Na2IrO3 are large and ferromagnetic: insights from ab initio quantum chemistry calculations"
date: 2014-01-01
publishDate: 2020-04-29T14:06:00.736029Z
authors: ["Vamshi M Katukuri", "S Nishimoto", "V Yushankhai", "A Stoyanova", "H Kandpal", "Sungkyun Choi", "R Coldea", "I Rousochatzakis", "L Hozoi", "Jeroen van den Brink"]
publication_types: ["2"]
abstract: ""
featured: true
publication: "*New J. Phys.*"
url_pdf: "http://stacks.iop.org/1367-2630/16/i=1/a=013056?key=crossref.72323fbb61535dd18d5c53e97e498b0c"
categories: [Iridates]
tags: [Anisotropic Exchange, Na2IrO3, Kitaev interactions, Kitaev Model, Kitaev Materials]
doi: "10.1088/1367-2630/16/1/013056"
---

