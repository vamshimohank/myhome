---
title: "Coherent generation of symmetry-forbidden phonons by light-induced electron-phonon interactions in magnetite"
date: 2017-09-01
publishDate: 2020-04-29T14:06:00.745619Z
authors: ["S. Borroni", "E. Baldini", "V. M. Katukuri", "A. Mann", "K. Parlinski", "D. Legut", "C. Arrell", "F. van Mourik", "J. Teyssier", "A. Kozlowski", "P. Piekarz", "O. V. Yazyev", "A. M. Oleś", "J. Lorenzana", "F. Carbone"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. B*"
url_pdf: "https://link.aps.org/doi/10.1103/PhysRevB.96.104308"
doi: "10.1103/PhysRevB.96.104308"
---

