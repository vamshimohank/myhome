---
title: "J 1 − J 2 square lattice antiferromagnetism in the orbitally quenched insulator MoOPO 4"
date: 2017-07-01
publishDate: 2020-04-29T14:06:00.742544Z
authors: ["L. Yang", "M. Jeong", "P. Babkevich", "V. M. Katukuri", "B. Náfrádi", "N. E. Shaik", "A. Magrez", "H. Berger", "J. Schefer", "E. Ressouche", "M. Kriener", "I. Živković", "O. V. Yazyev", "L. Forró", "H. M. Rønnow"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Phys. Rev. B*"
url_pdf: "http://link.aps.org/doi/10.1103/PhysRevB.96.024445"
doi: "10.1103/PhysRevB.96.024445"
---

