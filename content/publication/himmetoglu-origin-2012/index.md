---
title: "Origin of magnetic interactions and their influence on the structural properties of Ni$_2$MnGa and related compounds"
date: 2012-05-01
publishDate: 2020-04-29T14:06:00.732673Z
authors: ["Burak Himmetoglu", "Vamshi M Katukuri", "Matteo Cococcioni"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*J. Phys.: Condens. Matter*"
url_pdf: "http://stacks.iop.org/0953-8984/24/i=18/a=185501?key=crossref.f1b8987b89c7b5bb72993a3613b1b165"
doi: "10.1088/0953-8984/24/18/185501"
---

