---
# Display name
title: Dr. Vamshi M. Katukuri

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Senior Scientist 

# Organizations/Affiliations
organizations:
- name: Fraunhofer Institute for Industrial Engineering (IAO), Stuttgart
  url: "https://www.digital.iao.fraunhofer.de/en/leistungen/Quantencomputing.html"

# Short bio (displayed in user profile at end of posts)
bio: Scientist with interests in quantum physics, computing and sport enthusiast

interests:
- Quantum Computing
- Quantum Chemistry on quantum computers
- Fault tollerant quantum algorithms
- Teaching and training of qauntum technologies 

education:
  courses:
  - course: PhD in Physics
    institution: Dresden Technical University
    year: 2015
  - course: MS in Material Science Engineering
    institution: University of Minnesota
    year: 2011
  - course: MTech in Computational methods in Physics
    institution:  University of Hyderabad
    year: 2007
  - course: MSc in Physics
    institution:  Sri Satya Sai Institute of Higher Learning
    year: 2005
  - course: BSc in Physics
    institution:  Sri Satya Sai Institute of Higher Learning
    year: 2003

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
#  link: '#contact'  # For a direct email link, use "mailto:v.katukuri@fkf.mpg.de".
  link: "mailto:vamshi.katukuri@iao.fraunhofer.de" #.'#contact'  # For a direct email link, use 
- icon: graduation-cap
  icon_pack: fas
  link: https://scholar.google.ch/citations?hl=en&user=8V8H14kAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/vamshimohank
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0001-9355-0594
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/katuk001
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "vamshi.katukuri@iao.fraunhofer.de"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

I am a computational physicst working at the interface of quantum chemistry, quantum physics and quantum computing.
My research is focused on developing quantum algorithms for simulating quantum mechanical systems  e.g, molecules and solid state materials. 
More recently I started working towards Fault tolerrant quantum computing. 
<? I use and develop several ab initio computational methods/techniques based on wavefunction quantum chemistry as well as density functional theory. 
 Material systems I am mostly involved with are strongly correlated oxides/halides and frustrated quantum magnets.
 Currently, I am employing Full CI Quantum Monte Carlo (FCIQMC) and Density Matrix Renormalization group (DMRG) methods for computing the ground and  excited state properties to investigate the fundamental properties of the newly discovered superconductors based on nickel oxides. 
 Another project I am working on is realted to solving lattice models with frustrated interactions using FCIQMC.
 Besides, I am greatly enthusiastic about learning (and hopefully contributing towards) quantum computing and machine learning applications. ?>

Otherwise, I have a great appetite for non-professional sport, particularly running, cycling and tennis. 
I am an avid motorbiker and enjoy horse-back riding as well. 
