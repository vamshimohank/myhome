---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Possible Spin Liquid ground state in square-lattice double perovskite cuprates"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-04-29T22:16:55+02:00
lastmod: 2020-04-29T22:16:55+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
In a simple magnet, the magnetic moments, also called spins, arrange themselves below a certain temperature often called critical temperature T$_{c}$.
Based on how this alingment is, there are several "ordered" ground states observed in materials, e.g. in ferromagents 
the moments arrange parallel to each other. However, it is possible that the spins do not orient mutually, i.e "disordered".

The Nobel laureate [Philip Anderson](https://www.nobelprize.org/prizes/physics/1977/anderson/biographical/)[^1], incidentally he died [last month](https://www.sciencemag.org/news/2020/03/philip-anderson-legendary-theorist-whose-ideas-shaped-modern-physics-dies#), theoretically proposed the existence of such a 
disordered state over forty years ago and named it as a "quantum spin liquid".
It is so called because magnetic moments behave like a liquid and do not freeze or order even at absolute zero temperature.
Interestingly, not much is understood about this particular state of matter. However, predictions tell that such materials exhibit several extraordinary properties (more on that in a different post).

According to Anderson, a QSL state can be acheived in a material with a frustrated geometric lattice, such as a triangular lattice, and nearest neighbor antiferromagentic interactions, or on a lattice with competing nearest and next-nearest neighbor interactions e.g on a square lattice system [^2]. 
In 2007, a material based on Herbetsmithite mineral with spin-half moments sitting on a kagome lattice was found to not show any magnetic ordering down to 50 mK (-269.95 $^\circ$C)[^3]. The current consensus is that this material is indeed in a QSL state. The geometry of a [Kagome lattice](https://en.wikipedia.org/wiki/Trihexagonal_tiling#Kagome) is akin to a triangular lattice but with hexagonal connects. 

{{< figure library="true" src="kagome-lattice-with-spin-frustration.png" title="Spin frustration on a Kagome lattice" lightbox="true" >}}

However, a quantum disordered magnet on a square lattice system has not been realised. This is mainly due to the difficulty in finding a material with appropriate magnetic interactions -- $J_2/J_1 \approx \pm 0.5$ -- that is required for the spins to behave like a liquid. In 2018, it was observed in double perovskite materials of the type A$_2$BB$^\prime$O$_6$ that having two different B$^\prime$ species could result in competing exchange couplings[^4].
We recently explained how frustrated magnetic interactions manifest in Sr$_2$CuTe$_{0.5}$W$_{0.5}$O$_6$ that could result in a spin liquid ground state[^5]. However, the story is not yet complete as there are no firm evidences to pinpointedly say that the ground state is a QSL. Further experiments, e.g. neutron scattering measurements, are required to carefully examine to rule out other types of ground states. 


[^1]: Philip W. Anderson – Biographical. [NobelPrize.org](https://www.nobelprize.org/prizes/physics/1977/anderson/biographical/).
[^2]: P. W. Anderson "The Resonating Valence Bond State in La$_2$CuO$_4$ and Superconductivity" [Science 235 1196 (1997)](https://science.sciencemag.org/content/235/4793/1196)
[^3]: J. Helton et al., "Spin Dynamics of the Spin-$1/2$ Kagome Lattice Antiferromagnet ZnCu$_3$(OH)$_6$Cl$_2$" [Phys. Rev. Lett. 98, 107204 (2007)](https://doi.org/10.1103/PhysRevLett.98.107204)
[^4]: O. Mustonen et al., Spin-liquid-like state in a spin-1/2 square-lattice antiferromagnet perovskite induced by $d^{10}$ − $d^0$ cation mixing, [Nat. Commun. 9, 1085 (2018)](https://doi.org/10.1038/s41467-018-03435-1)
[^5]: Vamshi M. Katukuri et al., Exchange Interactions Mediated by Nonmagnetic Cations in Double Perovskites, [Phys. Rev. Lett. 124, 077202 (2020)](https://doi.org/10.1103/PhysRevLett.124.077202)

```python
```
